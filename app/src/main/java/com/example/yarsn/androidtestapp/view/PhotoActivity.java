package com.example.yarsn.androidtestapp.view;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.yarsn.androidtestapp.R;
import com.example.yarsn.androidtestapp.contract.PhotoContract;
import com.example.yarsn.androidtestapp.data.Injection;
import com.example.yarsn.androidtestapp.data.MainAlertDialog;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.presenter.PhotoPresenter;
import com.google.gson.Gson;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

//Need to fix decode images - out of memory
public class PhotoActivity extends AppCompatActivity implements PhotoContract.View{

    private File userImage;
    private User user;
    private PhotoContract.ActionListener actionListener;

    @BindView(R.id.previewLayout)
    LinearLayout previewLayout;
    @BindView(R.id.layoutShowOnePhoto)
    LinearLayout layoutShowOnePhoto;
    @BindView(R.id.spinnerOnMakePhotoActivity)
    ProgressBar spinnerOnMakePhotoActivity;
    @BindView(R.id.showPhoto)
    ImageView showPhoto;
    private static final int CAMERA_OPEN = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);

        user = new Gson().fromJson(getIntent().getStringExtra("user"), User.class);
        actionListener = new PhotoPresenter(this, Injection.provideRepository());
    }

    @OnClick(R.id.sendData)
    void sendData() {
        if (userImage != null) {
            spinnerOnMakePhotoActivity.setVisibility(View.VISIBLE);
            actionListener.updateUser(user, userImage);
        } else {
            MainAlertDialog.show(this, getString(R.string.msg_error), "You did't do the photo");
        }
    }

    @OnClick(R.id.imagePic)
    void getImage() {
        actionListener.takePhoto();
    }

    @OnClick(R.id.takeAnotherPhoto)
    void takeAnotherPhoto() {
        actionListener.takePhoto();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                MainAlertDialog.show(getApplicationContext(), getString(R.string.msg_error), "\n***\n"+e);
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                userImage = imageFile;

                showPreviewImageLayout(imageFile);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(PhotoActivity.this);
                    if (photoFile != null)
                        photoFile.delete();
                }
            }
        });
    }

    @Override
    public void openCamera() {
        EasyImage.openCamera(this, CAMERA_OPEN);
    }

    @Override
    public void showSpinner(boolean isShowSpinner) {
        spinnerOnMakePhotoActivity.setVisibility(View.GONE);
        if (isShowSpinner)
            spinnerOnMakePhotoActivity.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSuccessAlertDialog(String message) {
        MainAlertDialog.show(this, getString(R.string.msg_okay), message);
    }

    @Override
    public void showErrorAlertDialog(String message) {
        MainAlertDialog.show(this, getString(R.string.msg_error), message);
    }

    @Override
    public void showPreviewImageLayout(File imageFile) {
        previewLayout.setVisibility(View.GONE);
        layoutShowOnePhoto.setVisibility(View.VISIBLE);

        showPhoto.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
    }
}
