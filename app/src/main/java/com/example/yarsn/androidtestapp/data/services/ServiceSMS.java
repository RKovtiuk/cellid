package com.example.yarsn.androidtestapp.data.services;

import android.os.Bundle;
import android.telephony.SmsMessage;

import com.example.yarsn.androidtestapp.contract.VerificationContract;

public class ServiceSMS {

    public static String message, code;
    public static SmsMessage currentMessage;

    public static void getSMS(Bundle bundle, VerificationContract.View view){
        final Object[] pdusObjs = (Object[]) bundle.get("pdus");
        for (Object pdusObj : pdusObjs) {

            currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj);
            message = currentMessage.getDisplayMessageBody();
            code = message.substring(message.indexOf(":") + 1, message.length()).trim();

            view.setCodeToField(code);
        }
    }
}
