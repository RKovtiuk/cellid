package com.example.yarsn.androidtestapp.data.repositories;

import android.support.annotation.NonNull;

import com.example.yarsn.androidtestapp.data.repositories.impl.RepositoryImpl;
import com.example.yarsn.androidtestapp.data.services.ServiceUser;

public class RepositoryFactory {
    private RepositoryFactory() {
    }

    private static Repository repository = null;

    public synchronized static Repository newRepositoryInstance(@NonNull ServiceUser service) {
        if (null == repository)
            repository = new RepositoryImpl(service);
        return repository;
    }
}
