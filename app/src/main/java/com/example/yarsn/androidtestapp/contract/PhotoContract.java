package com.example.yarsn.androidtestapp.contract;

import com.example.yarsn.androidtestapp.data.dto.User;

import java.io.File;

public interface PhotoContract {

    interface ActionListener {
        void takePhoto();
        void updateUser(User user, File photo);
    }

    interface View {
        void openCamera();
        void showSpinner(boolean isShowSpinner);
        void showSuccessAlertDialog(String message);
        void showErrorAlertDialog(String message);
        void showPreviewImageLayout(File fIle);

    }
}
