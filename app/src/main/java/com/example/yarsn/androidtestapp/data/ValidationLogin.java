package com.example.yarsn.androidtestapp.data;

import android.content.Context;
import android.widget.EditText;

import com.mobsandgeeks.saripaar.ValidationError;

import java.util.List;

public class ValidationLogin {
    public static void showError(Context context, List<ValidationError> errors) {
        for (ValidationError error : errors) {
            ((EditText) error.getView()).setError(error.getCollatedErrorMessage(context));
        }
    }
}
