package com.example.yarsn.androidtestapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.yarsn.androidtestapp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoDialog extends BaseAdapter{

    private LayoutInflater layoutInflater;

    public InfoDialog(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InfoHolder viewHolder;
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.dialog_photo_info, parent, false);
            viewHolder = new InfoHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (InfoHolder) view.getTag();
        }

        Context context = parent.getContext();
        switch (position) {
            case 0:
                viewHolder.dialogTextView.setText(context.getString(R.string.passport));
                break;
            case 1:
                viewHolder.dialogTextView.setText(context.getString(R.string.other_doc));
                break;
        }

        return view;
    }

    class InfoHolder {
        @BindView(R.id.dialogInfoText) TextView dialogTextView;
        @BindView(R.id.dialogInfoImage) ImageView dialogImageView;

        public InfoHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
