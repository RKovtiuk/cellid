package com.example.yarsn.androidtestapp.contract;

import com.example.yarsn.androidtestapp.data.dto.User;

import java.io.File;

public interface InfoContract {

    interface ActionListener{
        void takeDataOfBirth();
        void takeOnePhoto(File file);
        void takePhotos();
        void clearPhotos();
        void proceed(User user);
    }

    interface View{
        void openInfoPhotoDialog();
        void addViewTakeAnotherPhoto();
        void openPhotoActivity(User user);
        void showPreviewImageLayout(File file);
        void showImageGallery();
        void clearAllPhoto();
        void showCalendar();
    }
}
