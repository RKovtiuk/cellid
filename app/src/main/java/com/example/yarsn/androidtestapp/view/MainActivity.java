package com.example.yarsn.androidtestapp.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.yarsn.androidtestapp.R;
import com.example.yarsn.androidtestapp.contract.MainContract;
import com.example.yarsn.androidtestapp.data.Injection;
import com.example.yarsn.androidtestapp.data.MainAlertDialog;
import com.example.yarsn.androidtestapp.data.ValidationLogin;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.presenter.MainPresenter;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View, Validator.ValidationListener {

    @NotEmpty
    @BindView(R.id.phoneNumber)
    EditText phone;
    @BindView(R.id.progressBar)
    ProgressBar spinner;
    private MainContract.ActionsListener actionsListener;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        actionsListener = new MainPresenter(this, Injection.provideRepository());
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.sign_in)
    public void signIn(){
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        actionsListener.signIn(phone.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Toast.makeText(this, "Illegal number", Toast.LENGTH_LONG).show();
//        ValidationLogin.showError(this, errors);
    }

    @Override
    public void showAlertDialogWhenLoginFail(String msg) {
        Toast.makeText(this, "Login was failed", Toast.LENGTH_SHORT);
//        MainAlertDialog.show(this, getString(R.string.msg_error), message);
    }

    @Override
    public void showSpinner(boolean showSpinner) {
        spinner.setVisibility(View.GONE);
        if(showSpinner)
            spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void openVerificationActivity(User user) {
        Intent intent = new Intent(MainActivity.this, VerificationActivity.class);
        intent.putExtra("user", new Gson().toJson(user));
        startActivity(intent);
    }
}
