package com.example.yarsn.androidtestapp.presenter;

import android.support.annotation.NonNull;

import com.example.yarsn.androidtestapp.contract.PhotoContract;
import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.data.repositories.Repository;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PhotoPresenter implements PhotoContract.ActionListener {
    private PhotoContract.View view;
    private Repository repository;

    public PhotoPresenter(@NonNull PhotoContract.View view,@NonNull Repository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void takePhoto() {
        view.openCamera();
    }

    @Override
    public void updateUser(User user, File photo) {
        view.showSpinner(true);

        HashMap<String, RequestBody> userDocumentsPhoto = getUserDocumentsPhoto(user.getImages());
        HashMap<String, RequestBody> userPhoto = getUserPhoto(photo);

        repository.updateProfile(
                user.getSessionToken(),
                user.getFirstName(),
                user.getSurname(),
                user.getDateOfBirth(),
                user.getAddressOfBirth(),
                userDocumentsPhoto,
                userPhoto,
                new Repository.UpdateCallback() {
                    @Override
                        public void updateSuccess(Response response) {
                            view.showSpinner(false);
                            view.showSuccessAlertDialog(response.getMessage());
                        }

                        @Override
                        public void updateFail(String error) {
                            view.showSpinner(false);
                            view.showErrorAlertDialog(error);
                        }
                });
    }

    private HashMap<String, RequestBody> getUserDocumentsPhoto(File[] images) {
        HashMap<String, RequestBody> userDocumentsPhoto = new HashMap<>();
        for (File image : images) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            userDocumentsPhoto.put("document_photos[]\"; filename=\"" + image.getName(), fileBody);
        }
        return userDocumentsPhoto;
    }

    private HashMap<String, RequestBody> getUserPhoto(File image) {
        HashMap<String, RequestBody> userPhoto = new HashMap<>();
        RequestBody photo = RequestBody.create(MediaType.parse("multipart/form-data"), image);
        userPhoto.put("photo\"; filename=\"" + image.getName(), photo);
        return userPhoto;
    }
}
