package com.example.yarsn.androidtestapp.view;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.yarsn.androidtestapp.R;
import com.example.yarsn.androidtestapp.adapter.InfoDialog;
import com.example.yarsn.androidtestapp.contract.InfoContract;
import com.example.yarsn.androidtestapp.data.Injection;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.presenter.InfoPresenter;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class InfoActivity extends AppCompatActivity implements InfoContract.View, Validator.ValidationListener{

    private User user;
    private List<File> images = new ArrayList<File>();
    private InfoContract.ActionListener actionListener;
    private Validator validator;

    private static final int ONE_PHOTO = 0;
    private static final int MORE_PHOTO = 1;

    @NotEmpty @BindView(R.id.firsName)
    EditText firstName;
    @NotEmpty @BindView(R.id.secondName)
    EditText secondName;
    @NotEmpty @BindView(R.id.placeOfBirth)
    EditText placeOfBirth;
    @NotEmpty @BindView(R.id.dateOfBirth)
    EditText dateOfBirth;

    @BindView(R.id.previewImage)
    ImageView previewImage;
    @BindView(R.id.imageGallery)
    LinearLayout imageGallery;
    @BindView(R.id.imageGalleryHSV)
    HorizontalScrollView imageGalleryHSV;
    @BindView(R.id.previewLayout)
    RelativeLayout previewLayout;
    @BindView(R.id.layoutShowOnePhoto)
    LinearLayout layoutShowOnePhoto;
    @BindView(R.id.imageGalleryLayout)
    LinearLayout imageGalleryLayout;
    @BindView(R.id.onePhoto)
    ImageView onePhoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);

        user = new Gson().fromJson(getIntent().getStringExtra("user"), User.class);
        validator = new Validator(this);
        validator.setValidationListener(this);
        actionListener = new InfoPresenter(this, Injection.provideRepository());
    }

    @Override
    public void openInfoPhotoDialog() {
        DialogPlus dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ListHolder())
                .setHeader(R.layout.dialog_photo_info)
                .setContentHolder(new ListHolder())
                .setAdapter(new InfoDialog(this))
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        dialog.dismiss();

                        switch (position) {
                            case ONE_PHOTO:
                                EasyImage.openCamera(InfoActivity.this, ONE_PHOTO);
                                break;
                            case MORE_PHOTO:
                                EasyImage.openCamera(InfoActivity.this, MORE_PHOTO);
                                break;
                        }
                    }
                })
                .setExpanded(false)
                .create();
        dialog.show();
    }

    @Override
    public void showPreviewImageLayout(File file) {
        previewLayout.setVisibility(View.GONE);
        layoutShowOnePhoto.setVisibility(View.VISIBLE);
        onePhoto.setImageBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));
    }

    @Override
    public void showImageGallery() {
        previewLayout.setVisibility(View.GONE);
        imageGalleryLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void clearAllPhoto() {
        imageGallery.removeAllViews();
        images.clear();

    }

    @Override
    public void showCalendar() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                dateOfBirth.setText(simpleDateFormat.format(calendar.getTime()));
            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(InfoActivity.this, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @OnClick(R.id.previewImage)
    void makePhoto() {
        openInfoPhotoDialog();
    }

    @OnClick(R.id.proceed)
    void proceed() {
        validator.validate();
    }

    @OnClick(R.id.dateOfBirth)
    void openDatePickerDialog() {
        actionListener.takeDataOfBirth();
    }

    @OnClick(R.id.takeAnotherPhoto)
    void takeAnotherPhoto() {
        EasyImage.openCamera(InfoActivity.this, ONE_PHOTO);
    }

    @OnClick(R.id.clearAllPhoto)
    void clearPhoto() {
        actionListener.clearPhotos();
    }

    @Override
    public void onValidationSucceeded() {

        File[] files = images.toArray(new File[images.size()]);

        user.setFirstName(firstName.getText().toString());
        user.setSurname(secondName.getText().toString());
        user.setDateOfBirth(dateOfBirth.getText().toString());
        user.setAddressOfBirth(placeOfBirth.getText().toString());
        user.setSessionToken(user.getSessionToken());
        user.setImages(files);

        actionListener.proceed(user);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError validationError : errors) {
            ((EditText) validationError.getView()).setError(validationError.getCollatedErrorMessage(this));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                switch (type) {
                    case ONE_PHOTO:
                        images.clear();
                        images.add(imageFile);
                        actionListener.takeOnePhoto(imageFile);
                        break;
                    case MORE_PHOTO:
                        images.add(imageFile);
                        actionListener.takePhotos();
                        createView(imageFile);
                        break;
                }

            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(InfoActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void createView(File file) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_image, null, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));

        if (imageGallery.getChildCount() == 0) {
            imageGallery.addView(view);
            addViewTakeAnotherPhoto();
        } else {
            imageGallery.removeViewAt(imageGallery.getChildCount() - 1);
            imageGallery.addView(view);
            addViewTakeAnotherPhoto();
            imageGalleryHSV.post(new Runnable() {
                @Override
                public void run() {
                    imageGalleryHSV.scrollTo(imageGallery.getChildAt(imageGallery.getChildCount() - 1).getRight(), 0);
                }
            });

        }
    }

    @Override
    public void addViewTakeAnotherPhoto() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_another_photo, null, false);
        TextView takeAnotherPhoto = (TextView) view.findViewById(R.id.makePhoto);
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.dialogAnotherPhoto);
        relativeLayout.setGravity(RelativeLayout.CENTER_HORIZONTAL);
        takeAnotherPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openCamera(InfoActivity.this, MORE_PHOTO);
            }
        });
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imageGallery.addView(view);
    }

    @Override
    public void openPhotoActivity(User user) {
        Intent intent = new Intent(this, PhotoActivity.class);
        intent.putExtra("user", new Gson().toJson(user));
        startActivity(intent);
    }
}
