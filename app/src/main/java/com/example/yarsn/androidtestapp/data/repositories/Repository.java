package com.example.yarsn.androidtestapp.data.repositories;

import android.support.annotation.NonNull;
import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.dto.User;

import java.util.Map;

import okhttp3.RequestBody;

public interface Repository {
    void login(@NonNull String phone, @NonNull Repository.LoginCallback callback);

    void verification(@NonNull String phone, @NonNull String code, @NonNull Repository.VerificationCallback callback);

    void resendCode(@NonNull String phone,@NonNull Repository.CodeCallback callback);

    void updateProfile(@NonNull String sessionToken, @NonNull String firstName, @NonNull String surName,
                       @NonNull String dataOfBirth, @NonNull String addressOfBirth, @NonNull Map<String, RequestBody> documentsOfPhoto,
                       @NonNull Map<String,RequestBody> photo, Repository.UpdateCallback callback);

    //callback
    interface CodeCallback {
        void codeSuccess(Response response);
        void codeFail(String error);
    }

    interface LoginCallback {
        void loginSuccess(User user);
        void loginFail(String error);
    }

    interface UpdateCallback {
        void updateSuccess(Response response);
        void updateFail(String error);
    }

    interface VerificationCallback {
        void verificationSuccess(Response response);
        void verificationFail(String error);
    }
    //
}
