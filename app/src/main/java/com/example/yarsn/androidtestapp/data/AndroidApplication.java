package com.example.yarsn.androidtestapp.data;


import android.app.Application;

import com.example.yarsn.androidtestapp.data.services.ServiceHTTP;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AndroidApplication extends Application {
    public static final String SERVER_URI = "http://31.131.16.243:83/api/v1/";
    public static ServiceHTTP service;
    public static Retrofit retrofit;


    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_URI)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        service = retrofit.create(ServiceHTTP.class);
    }

    public static ServiceHTTP getServiceHTTP(){
        return service;
    }
}
