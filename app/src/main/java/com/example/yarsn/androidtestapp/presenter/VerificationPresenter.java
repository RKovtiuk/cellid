package com.example.yarsn.androidtestapp.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.yarsn.androidtestapp.contract.VerificationContract;
import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.repositories.Repository;
import com.example.yarsn.androidtestapp.data.services.ServiceSMS;

public class VerificationPresenter implements VerificationContract.ActionListener {

    private final VerificationContract.View view;
    private final Repository repository;

    public VerificationPresenter(@NonNull VerificationContract.View mView, Repository mRepository) {
        this.view = mView;
        this.repository = mRepository;
    }

    public BroadcastReceiver getBroadcast(final VerificationContract.ActionListener actionListener, BroadcastReceiver broadcast) {
        broadcast = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                final Bundle bundle = intent.getExtras();
                try {
                    if (bundle != null) {
                        actionListener.getSMS(bundle);
                    }
                } catch (Exception e) {
                    Log.e("SMS Receiver", "Error SMS Receiver" + e);
                }
            }
        };

        return broadcast;
    }

    @Override
    public void getSMS(Bundle bundle) {
        ServiceSMS.getSMS(bundle, view);
    }

    @Override
    public void verification(String phone, String code) {
        view.showSpinner(false);
        repository.verification(phone, code, new Repository.VerificationCallback() {
            @Override
            public void verificationSuccess(Response response) {
                response.getSessionToken();
                view.showSpinner(false);
                view.openDetailActivity(response.getSessionToken());
            }

            @Override
            public void verificationFail(String error) {
                view.showSpinner(false);
                view.openDialogError(error);
            }
        });
    }

    @Override
    public void resendCode(String phone) {
        view.showSpinner(true);
        repository.resendCode(phone, new Repository.CodeCallback() {
            @Override
            public void codeSuccess(Response response) {
                view.showSpinner(false);
            }

            @Override
            public void codeFail(String error) {
                view.showSpinner(false);
                view.openDialogError(error);
            }
        });
    }
}
