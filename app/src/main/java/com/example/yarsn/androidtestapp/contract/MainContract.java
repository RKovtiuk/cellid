package com.example.yarsn.androidtestapp.contract;

import com.example.yarsn.androidtestapp.data.dto.User;

public interface MainContract {

    interface ActionsListener{
        void signIn(String phone);
    }

    interface View{
        void showAlertDialogWhenLoginFail(String msg);
        void showSpinner(boolean showSpinner);
        void openVerificationActivity(User user);
    }
}
