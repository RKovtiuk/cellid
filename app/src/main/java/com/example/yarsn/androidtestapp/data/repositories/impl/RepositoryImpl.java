package com.example.yarsn.androidtestapp.data.repositories.impl;

import android.support.annotation.NonNull;

import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.data.repositories.Repository;
import com.example.yarsn.androidtestapp.data.services.ServiceUser;
import java.util.Map;
import okhttp3.RequestBody;

public class RepositoryImpl implements Repository {

    private ServiceUser serviceUser;

    public RepositoryImpl(@NonNull ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    @Override
    public void verification(@NonNull String phone, @NonNull String code, @NonNull final Repository.VerificationCallback callback) {
        serviceUser.verification(phone, code, new ServiceUser.ServiceCallback<Response>() {
            @Override
            public void loadSuccess(Response response) {
                callback.verificationSuccess(response);
            }

            @Override
            public void loadFail(String error) {
                callback.verificationFail(error);
            }
        });
    }

    @Override
    public void login(@NonNull String phone, @NonNull final Repository.LoginCallback callback) {
        serviceUser.login(phone, new ServiceUser.ServiceCallback<User>() {
            @Override
            public void loadSuccess(User user) {
                callback.loginSuccess(user);
            }

            @Override
            public void loadFail(String error) {
                callback.loginFail(error);
            }
        });
    }

    @Override
    public void resendCode(@NonNull String phone, @NonNull final Repository.CodeCallback callback) {
         serviceUser.code(phone, new ServiceUser.ServiceCallback<Response>() {
             @Override
             public void loadSuccess(Response response) {
                 callback.codeSuccess(response);
             }

             @Override
             public void loadFail(String error) {
                 callback.codeFail(error);
             }
         });
    }

    @Override
    public void updateProfile(@NonNull String sessionToken, @NonNull String firstName, @NonNull String secondName, @NonNull String dataOfBirth, @NonNull String addressOfBirth, @NonNull Map<String, RequestBody> documentsOfPhoto, @NonNull Map<String, RequestBody> photo, final Repository.UpdateCallback callback) {
        serviceUser.update(sessionToken, firstName, secondName, dataOfBirth, addressOfBirth, documentsOfPhoto, photo, new ServiceUser.ServiceCallback<Response>() {
            @Override
            public void loadSuccess(Response response) {callback.updateSuccess(response);}

            @Override
            public void loadFail(String error) {
                callback.updateFail(error);
            }
        });
    }
}
