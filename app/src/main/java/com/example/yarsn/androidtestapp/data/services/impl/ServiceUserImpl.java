package com.example.yarsn.androidtestapp.data.services.impl;

import android.util.Log;

import com.example.yarsn.androidtestapp.data.AndroidApplication;
import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.data.services.ServiceUser;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ServiceUserImpl implements ServiceUser {
    @Override
    public void login(String phone, final ServiceCallback<User> callback) {
        AndroidApplication.getServiceHTTP().login(phone).enqueue(new Callback<User>(){
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                callback.loadFail(t.getMessage());
            }

            @Override
            public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                if(response.isSuccessful()){
                    callback.loadSuccess(response.body());
                } else {
                    callback.loadFail(response.message());
                }
            }
        });
    }

    @Override
    public void verification(String phone, String code, final ServiceCallback<Response> callback) {
        AndroidApplication.getServiceHTTP().verify(phone,code).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    callback.loadSuccess(response.body());
                }else {
                    callback.loadFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                callback.loadFail(t.getMessage());
            }
        });
    }

    @Override
    public void code(String phone, final ServiceCallback<Response> callback) {
        AndroidApplication.getServiceHTTP().resendCode(phone).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    callback.loadSuccess(response.body());
                } else {
                    callback.loadFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                callback.loadFail(t.getMessage());
            }
        });
    }

    @Override
    public void update(String sessionToken, String firstName, String secondName, String dataOfBirth, String addressOfBirth, Map<String, RequestBody> documentPhoto, Map<String, RequestBody> photo, final ServiceCallback<Response> callback) {
        AndroidApplication.getServiceHTTP().update(sessionToken,firstName,secondName,dataOfBirth,addressOfBirth,documentPhoto,photo).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    callback.loadSuccess(response.body());
                }else {
                    callback.loadFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                callback.loadFail(t.getMessage());
            }
        });
    }
}
