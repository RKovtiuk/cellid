package com.example.yarsn.androidtestapp.data.services;

import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.dto.User;

import java.util.Map;

import okhttp3.RequestBody;

public interface ServiceUser {
    void login(String phone, ServiceCallback<User> callback);
    void verification(String phone, String code, ServiceCallback<Response> callback);
    void code(String phone, ServiceCallback<Response> callback);
    void update(String sessionToken, String firstName, String secondName,
                String dataOfBirth, String addressOfBirth, Map<String, RequestBody> documentPhoto,
                Map<String, RequestBody> photo, ServiceCallback<Response> callback);

    //callback
    interface ServiceCallback<M> {
        void loadSuccess(M m);
        void loadFail(String error);
    }
    //
}
