package com.example.yarsn.androidtestapp.data.dto;

import java.io.Serializable;

public class Token implements Serializable{
    private String sessionToken;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
