package com.example.yarsn.androidtestapp.data.services;

import com.example.yarsn.androidtestapp.data.dto.Response;
import com.example.yarsn.androidtestapp.data.dto.User;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ServiceHTTP {

    @FormUrlEncoded
    @POST("login")
    Call<User> login(@Field("phone_number") String phone);

    @FormUrlEncoded
    @POST("resend_verification_token")
    Call<Response> resendCode(@Field("phone_number") String phone);

    @FormUrlEncoded
    @POST("verify")
    Call<Response> verify(@Field("phone_number") String phone,
                             @Field("verification_code") String verificationCode);

    @Multipart
    @POST("update")
    Call<Response> update(@Header("Session-Token") String token,
                             @Part("first_name") String firstName,
                             @Part("last_name") String lastName,
                             @Part("date_of_birth") String dateOfBirth,
                             @Part("place_of_birth") String placeOfBirth,
                             @PartMap() Map<String,RequestBody> documentPhotos,
                             @PartMap() Map<String,RequestBody> photo);
}
