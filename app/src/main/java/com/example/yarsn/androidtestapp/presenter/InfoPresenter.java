package com.example.yarsn.androidtestapp.presenter;

import android.support.annotation.NonNull;

import com.example.yarsn.androidtestapp.contract.InfoContract;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.data.repositories.Repository;

import java.io.File;

import static com.google.common.base.Preconditions.checkNotNull;

public class InfoPresenter implements InfoContract.ActionListener{

    private InfoContract.View view;
    private Repository repository;

    public InfoPresenter(@NonNull InfoContract.View mView, @NonNull Repository mRepository) {
        this.view = mView;
        this.repository = mRepository;
    }

    @Override
    public void takeOnePhoto(File file) {
        view.showPreviewImageLayout(file);
    }

    @Override
    public void takePhotos() {
        view.showImageGallery();
    }

    @Override
    public void clearPhotos() {
        view.clearAllPhoto();
    }

    @Override
    public void takeDataOfBirth() {
        view.showCalendar();
    }

    @Override
    public void proceed(User user) {
        view.openPhotoActivity(user);
    }

}
