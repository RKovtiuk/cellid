package com.example.yarsn.androidtestapp.view;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import static com.example.yarsn.androidtestapp.data.MainAlertDialog.*;
import com.example.yarsn.androidtestapp.R;
import com.example.yarsn.androidtestapp.contract.VerificationContract;
import com.example.yarsn.androidtestapp.data.Injection;
import com.example.yarsn.androidtestapp.data.ValidationLogin;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.presenter.VerificationPresenter;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener, VerificationContract.View, Validator.ValidationListener {

    //    @BindView(R.id.verificationCode)
    EditText verificationCode;
    @BindView(R.id.verificationToolbar)
    Toolbar toolbar;
    @BindView(R.id.verificationSpinner)
    ProgressBar spinner;
    private User user;
    private BroadcastReceiver broadcast;
    private VerificationContract.ActionListener actionListener;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
        actionListener = new VerificationPresenter(this, Injection.provideRepository());

        verificationCode = (EditText) findViewById(R.id.verificationCode);
        user = new Gson().fromJson(getIntent().getStringExtra("user"), User.class);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(this);

        broadcast = actionListener.getBroadcast(actionListener, broadcast);

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.goNext)
    void goNext() {
        actionListener.verification(user.getPhoneNumber(), verificationCode.getText().toString());
    }

    @Override
    public void showSpinner(boolean showSpinner) {
        if (showSpinner)
            spinner.setVisibility(View.VISIBLE);
        else
            spinner.setVisibility(View.GONE);
    }

    @Override
    public void openDialogSuccess(String msg) {
        show(this, "Success", msg);
    }

    @Override
    public void openDialogError(String msg) {
        show(this, "Error", msg);
    }

    @Override
    public void openDetailActivity(String token) {
        user.setSessionToken(token);
        Intent intent = new Intent(VerificationActivity.this, InfoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("user", new Gson().toJson(user));
        startActivity(intent);
    }

    @Override
    public void setCodeToField(String code) {
        verificationCode.setText(code);
    }

    @Override
    public void onValidationSucceeded() {
        String cleanCode = verificationCode.getText().toString().replace("-", "");
        actionListener.verification(user.getPhoneNumber(), cleanCode);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationLogin.showError(this, errors);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.resend_code) {
            actionListener.resendCode(user.getPhoneNumber());
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (broadcast != null) {
            unregisterReceiver(broadcast);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcast, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }

    @Override
    public void onClick(View view) {
        onBackPressed();
    }
}
