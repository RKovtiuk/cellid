package com.example.yarsn.androidtestapp.contract;

import android.content.BroadcastReceiver;
import android.os.Bundle;

public interface VerificationContract {

    interface ActionListener{
        BroadcastReceiver getBroadcast(final VerificationContract.ActionListener actionListener, BroadcastReceiver broadcast);
        void getSMS(Bundle bundle);
        void verification(String phone, String code);
        void resendCode(String phone);
    }

    interface View{
        void showSpinner(boolean showSpinner);
        void openDialogSuccess(String msg);
        void openDialogError(String msg);
        void openDetailActivity(String token);
        void setCodeToField(String code);
    }
}
