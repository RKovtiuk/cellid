package com.example.yarsn.androidtestapp.data.dto;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.io.Serializable;

public class User extends Response implements Serializable{
//    @SerializedName("first_name")
    private String firstName;
//    @SerializedName("surname")
    private String surname;
//    @SerializedName("phone_number")
    private String phoneNumber;
//    @SerializedName("date_of_birth")
    private String dateOfBirth;
//    @SerializedName("address_of_birth")
    private String addressOfBirth;
//    @SerializedName("document_photos")
    private String[] documentPhotos;
//    @SerializedName("photo")
    private String photo;

//    @SerializedName("created_at")
    private String createdAt;
//    @SerializedName("update_at")
    private String updatedAt;
//    @SerializedName("status")
    private String status;
//    @SerializedName("reject_reason")
    private String rejectReason;

    @SerializedName("Session-Token")
    private String sessionToken;

    private File[] images;

    public User(String token, String firstName, String surname, String dateOfBirth, String addressOfBirth, File[] images) {
        this.firstName = firstName;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.addressOfBirth = addressOfBirth;
        this.images = images;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddressOfBirth() {
        return addressOfBirth;
    }

    public void setAddressOfBirth(String addressOfBirth) {
        this.addressOfBirth = addressOfBirth;
    }

    public File[] getImages() {
        return images;
    }

    public void setImages(File[] images) {
        this.images = images;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String[] getDocumentPhotos() {
        return documentPhotos;
    }

    public void setDocumentPhotos(String[] documentPhotos) {
        this.documentPhotos = documentPhotos;
    }
}
