package com.example.yarsn.androidtestapp.presenter;

import android.support.annotation.NonNull;

import com.example.yarsn.androidtestapp.contract.MainContract;
import com.example.yarsn.androidtestapp.data.dto.User;
import com.example.yarsn.androidtestapp.data.repositories.Repository;
import com.google.common.base.Preconditions;

public class MainPresenter implements MainContract.ActionsListener{

    private final MainContract.View view;
    private final Repository repository;

    public MainPresenter(@NonNull MainContract.View mView, @NonNull Repository mRepository) {
        view = Preconditions.checkNotNull(mView, "MainView can't be null");
        repository = Preconditions.checkNotNull(mRepository, "Repository can't be null");
    }

    @Override
    public void signIn(String phone) {
        repository.login(phone, new Repository.LoginCallback() {
            @Override
            public void loginSuccess(User user) {
                view.showSpinner(false);
                view.openVerificationActivity(user);
            }

            @Override
            public void loginFail(String error) {
                view.showSpinner(false);
                view.showAlertDialogWhenLoginFail(error);
            }
        });
    }


}
