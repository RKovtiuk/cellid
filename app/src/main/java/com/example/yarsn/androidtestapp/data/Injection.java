package com.example.yarsn.androidtestapp.data;

import com.example.yarsn.androidtestapp.data.repositories.RepositoryFactory;
import com.example.yarsn.androidtestapp.data.repositories.Repository;
import com.example.yarsn.androidtestapp.data.services.impl.ServiceUserImpl;

public class Injection {
    public static Repository provideRepository(){
        return RepositoryFactory.newRepositoryInstance(new ServiceUserImpl());
    }
}
